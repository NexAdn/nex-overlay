# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="CA certificates for dn42"
HOMEPAGE="https://wiki.dn42.us/"
SRC_URI=""

LICENSE="ca-certificates-dn42"
SLOT="0"
KEYWORDS="~amd64 ~x86"

DEPEND="
	app-misc/ca-certificates
"
RDEPEND="${DEPEND}"
BDEPEND=""

S="${WORKDIR}"

src_install() {
	insinto /usr/share/ca-certificates/dn42
	doins "${FILESDIR}"/dn42-ca.crt
}

pkg_postinst() {
	if [[ -d "${EROOT}/usr/local/share/ca-certificates" ]]; then
		"${EROOT}"/usr/sbin/update-ca-certificates --root "${ROOT}"
	fi
}
