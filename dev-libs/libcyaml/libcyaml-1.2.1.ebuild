# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="C library for reading and writing YAML"
HOMEPAGE="https://github.com/tlsa/libcyaml/"
SRC_URI="https://github.com/tlsa/libcyaml/archive/refs/tags/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="ISC"
SLOT="0/$(ver_cut 1)"
KEYWORDS="~amd64 ~x86"

DEPEND=""
RDEPEND="${DEPEND}"
BDEPEND=""

PATCHES=(
	"${FILESDIR}/${P}-remove-hardcoded-cflags.patch"
)

# Variables used by Makefile
VARIANT=release

src_install() {
	emake \
		DESTDIR="${ED}" \
		PREFIX="${EPREFIX}/usr" \
		LIBDIR="$(get_libdir)" \
		install

	dodoc README.md CHANGES.md
}
