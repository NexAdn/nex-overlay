# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit kernel-build

MY_P=linux-${PV%.*}
GENPATCHES_P=genpatches-${PV%.*}-$(( ${PV##*.} + 6 ))

DESCRIPTION="Gentoo Kernel with nex' config"
HOMEPAGE="https://www.kernel.org/"
SRC_URI="
	https://cdn.kernel.org/pub/linux/kernel/v$(ver_cut 1).x/${MY_P}.tar.xz
	https://dev.gentoo.org/~mpagano/dist/genpatches/${GENPATCHES_P}.base.tar.xz
	https://dev.gentoo.org/~mpagano/dist/genpatches/${GENPATCHES_P}.extras.tar.xz
	https://gentoo-bin.nexadn.de/kernel/config_linux-${PV}-gentoo -> config-${PV}
"
S=${WORKDIR}/${MY_P}

LICENSE="GPL-2"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND=""
RDEPEND=""
BDEPEND=""

pkg_pretend() {
	ewarn "This kernel is meant for qemu virtual machines only."
	ewarn "One can not guarantee that the kernel will work on"
	ewarn "machines other that qemu VMs."

	kernel-install_pkg_pretend
}

src_prepare() {
	local PATCHES=(
		"${WORKDIR}"/*.patch
	)
	default

	cp "${DISTDIR}"/config-${PV} .config || die "Failed to copy config"
}
