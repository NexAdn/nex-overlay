# Copyright 2021-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit desktop

DESCRIPTION="Local platform for home banking"
HOMEPAGE="https://willuhn.de/"
SRC_URI="https://willuhn.de/products/jameica/releases/current/jameica/jameica-linux64-${PV}.zip"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="-* ~amd64"

DEPEND=""
RDEPEND="
	virtual/jre
"
BDEPEND=""

S="${WORKDIR}/jameica"

src_install() {
	insinto /opt/${PN}
	doins -r *

	make_desktop_entry "/bin/sh /opt/${PN}/jameica.sh" ${PN}
}
