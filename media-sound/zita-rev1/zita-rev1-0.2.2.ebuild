# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit toolchain-funcs

DESCRIPTION="Reverb effects plugin"
HOMEPAGE="https://kokkinizita.linuxaudio.org/"
SRC_URI="https://kokkinizita.linuxaudio.org/linuxaudio/downloads/${P}.tar.bz2"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"

DEPEND="
	dev-libs/libclthreads
	media-libs/libpng:=
	virtual/jack
	x11-libs/cairo
	x11-libs/libclxclient:=
	x11-libs/libXft
	x11-libs/libX11
"
RDEPEND="${DEPEND}"
BDEPEND=""

src_compile() {
	emake -C source CXX="$(tc-getCXX)" PREFIX="${EPREFIX}/usr"
}

src_install() {
	local myemakeargs=(
		DESTDIR="${D}"
		PREFIX="${EPREFIX}/usr"
		LIBDIR="${EPREFIX}/usr/$(get_libdir)"
	)
	emake -C source "${myemakeargs[@]}" install

	einstalldocs
}
