# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit meson xdg-utils

MY_PV="$(ver_cut 1-3)-alpha.$(ver_cut 4-6)"
MY_P="${PN}-${MY_PV}"

DESCRIPTION="A highly automated and intuitive digital audio workstation"
HOMEPAGE="https://www.zrythm.org/ https://git.zrythm.org/zrythm/zrythm"
SRC_URI="https://www.zrythm.org/releases/${MY_P}.tar.xz"

LICENSE="AGPL-3 CC-BY-SA-3.0 CC-BY-SA-4.0 FDL-1.3 GPL-3"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="carla doc graphviz guile +jack lsp-dsp opus portaudio rtaudio rtmidi sdl test"

DEPEND="
	app-arch/zstd:=
	dev-libs/libbacktrace
	dev-libs/libcyaml
	dev-libs/libpcre2:=
	dev-libs/json-glib
	dev-libs/xxhash
	dev-scheme/guile:=
	kde-frameworks/breeze-icons
	media-libs/libaudec
	media-libs/lilv
	media-libs/vamp-plugin-sdk
	sci-libs/fftw:=
	x11-libs/gtk+:3
	x11-libs/gtksourceview:3.0=
"
RDEPEND="${DEPEND}"
BDEPEND=""

S="${WORKDIR}/${MY_P}"

DOCS="CHANGELOG.md README.md THANKS TRADEMARKS.md"

src_configure() {
	local emesonargs=(
		"-Dappimage=false"
		"-Dcheck_updates=false"
		"-Dcompletions=true"
		"-Ddseg_font=false"
		"-Dextra_optimizations=false"
		"-Dextra_extra_optimizations=false"
		"-Dfallback_version=${MY_PV}"
		"-Dnative_build=false"
		"-Dprofiling=false"
		"-Dstatic_deps=false"
		"-Dstrict_flags=false"
		"-Dstrict_sphinx_opts=false"
		"-Dvalgrind=disabled"
		"-Dvamp_static=false"
		$(meson_feature carla)
		$(meson_use doc manpage)
		$(meson_use doc user_manual)
		$(meson_feature graphviz)
		$(meson_feature guile)
		$(meson_feature jack)
		$(meson_feature lsp-dsp lsp_dsp)
		$(meson_use opus)
		$(meson_feature portaudio)
		$(meson_feature rtaudio)
		$(meson_feature rtmidi)
		$(meson_feature sdl)
		$(meson_use test tests)
		$(meson_use test gui_tests)
	)
	meson_src_configure
}

src_install() {
	default
	meson_src_install

	exeinto /usr/$(get_libdir)/${PN}
	doexe scripts/meson-post-install.sh
}

pkg_postinst() {
	xdg_desktop_database_update

	MESON_INSTALL_DESTDIR_PREFIX="${EPREFIX}/usr" \
		/usr/$(get_libdir)/${PN}/meson-post-install.sh
}

pkg_postrm() {
	xdg_desktop_database_update
}
