# Copyright 2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

PYTHON_COMPAT=( python3_{8..11} )
inherit desktop python-single-r1

DESCRIPTION="NSM compatible session manager"
HOMEPAGE="https://www.laborejo.org/agordejo/ https://git.laborejo.org/lss/agordejo.git"
SRC_URI="https://www.laborejo.org/downloads/${P}.tar.gz"

LICENSE="GPL-3 CC-BY-ND-4.0"
SLOT="0"
KEYWORDS="~amd64"
REQUIRED_USE="${PYTHON_REQUIRED_USE}"

# Python dependencies are checked at build time, so they must be present
DEPEND="
	$(python_gen_cond_dep '
		dev-python/PyQt5[${PYTHON_USEDEP}]
		>=dev-python/pyxdg-0.27[${PYTHON_USEDEP}]
	')
"
# agordejo is _not_ compatible with non-session-manager
RDEPEND="
	${DEPEND}
	${PYTHON_DEPS}

	media-sound/new-session-manager
"

PATCHES=(
	"${FILESDIR}/${P}-fix-desktop-categories.patch"
)

src_prepare() {
	default
	sed -i "s/python3/${EPYTHON}/g" configure Makefile.in \
		|| die "Failed to set python3 to ${EPYTHON}"
	# The path to the CHANGELOG file does not respect ${PF} natively
	# Note: only do one replacement, as the first replacement is the correct one
	sed -i "s#\\(\"doc\": \\).*,\$#\\1\"/usr/share/doc/${PF}\",#" engine/start.py \
		|| die "Failed to set CHANGELOG path in sources"
}

src_install() {
	# The Makefile installs to wrong paths and compresses files.
	# Doing a manual install is easier

	newbin agordejo.bin agordejo
	newbin nsm-data.bin nsm-data

	domenu desktop/{agordejo-continue,nsm-data}.desktop
	newmenu desktop/desktop.desktop ${PN%-bin}.desktop
	pushd desktop/images > /dev/null || die
	for icon in *x*.png; do
		local size="${icon%x*}"
		newicon -s "${size}" "${icon}" "${PN%-bin}.png"
	done
	popd > /dev/null

	doman documentation/*.1

	# Order is important since documentation/out contains a CHANGELOG symlink
	dodoc -r documentation/out/*
	einstalldocs

	# CHANGELOG needs to be uncompressed for the software to work
	docompress -x "/usr/share/doc/${PF}/CHANGELOG"
}
