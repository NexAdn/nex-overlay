# Copyright 2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit non-build

DESCRIPTION="OSC based session management for Audio production"
KEYWORDS="~amd64"

RDEPEND="
	!media-sound/new-session-manager
"
