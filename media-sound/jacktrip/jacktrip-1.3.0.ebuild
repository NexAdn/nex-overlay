# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit meson

DESCRIPTION="Send JACK audio over a network"
HOMEPAGE="https://jacktrip.github.io/"
SRC_URI="https://github.com/jacktrip/jacktrip/archive/refs/tags/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86"

DEPEND="
	virtual/jack
	dev-qt/qtnetwork:5=
	dev-qt/qtcore:5=
"
RDEPEND="${DEPEND}"
BDEPEND=""
