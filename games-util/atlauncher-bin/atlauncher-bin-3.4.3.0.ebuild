# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit desktop java-pkg-2

DESCRIPTION="A launcher for Minecraft and Minecraft modpacks"
HOMEPAGE="https://atlauncher.com"
SRC_URI="
	https://github.com/ATLauncher/ATLauncher/releases/download/v${PV}/ATLauncher-${PV}.jar -> ${P}.jar
	https://github.com/ATLauncher/ATLauncher/raw/v3.4.3.0/packaging/aur/atlauncher-bin/atlauncher.png -> ${P}-icon.png
"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64 ~x86"

DEPEND=""
RDEPEND="virtual/jre:1.8"
BDEPEND=""

S="${WORKDIR}"

src_unpack() {
	cp "${DISTDIR}/${P}.jar" "${S}" || die
	cp "${DISTDIR}/${P}-icon.png" "${S}" || die
}

src_install() {
	java-pkg_newjar "${P}.jar" ${PN%-bin}.jar

	dobin "${FILESDIR}/${PN}"

	newicon ${P}-icon.png ${PN}.png

	make_desktop_entry ${PN} "ATLauncher" ${PN} Game
}
