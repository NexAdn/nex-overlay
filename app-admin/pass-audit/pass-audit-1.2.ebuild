# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

PYTHON_COMPAT=( python3_{8..11} )
DISTUTILS_USE_PEP517=setuptools
DISTUTILS_SINGLE_IMPL=1

inherit distutils-r1

DESCRIPTION="A pass extension for auditing you password repository"
HOMEPAGE="https://github.com/roddhjav/pass-audit"
SRC_URI="https://github.com/roddhjav/${PN}/releases/download/v${PV}/${P}.tar.gz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="test"

RDEPEND="
	$(python_gen_cond_dep '
		dev-python/requests[${PYTHON_USEDEP}]
		dev-python/zxcvbn[${PYTHON_USEDEP}]
	')
	>=app-admin/pass-1.7.0
"
DEPEND="
	test? (
		${RDEPEND}
		$(python_gen_cond_dep '
			dev-python/unittest-or-fail[${PYTHON_USEDEP}]
		')
	)
"
BDEPEND=""

RESTRICT="!test? ( test )"

distutils_enable_tests unittest

src_install() {
	einstalldocs
	distutils-r1_src_install

	cp -vR "${D}"/usr/lib/python*/site-packages/usr/* "${D}"/usr || die
	rm -r "${D}"/usr/lib/python*/site-packages/usr || die
}
