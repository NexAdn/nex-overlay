# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

PYTHON_COMPAT=( python3_{10..11} )
inherit python-single-r1 git-r3

DESCRIPTION="Fast block-level out-of-band BTRFS deduplication tool"
HOMEPAGE="https://github.com/lakshmipathi/dduper"
SRC_URI=""
EGIT_REPO_URI="https://github.com/lakshmipathi/dduper.git"

LICENSE="GPL-2"
SLOT="0"
IUSE=""
REQUIRED_USE="${PYTHON_REQUIRED_USE}"

DEPEND=""
RDEPEND="
	${PYTHON_DEPS}

	$(python_gen_cond_dep '
		dev-python/numpy[${PYTHON_USEDEP}]
		dev-python/PTable[${PYTHON_USEDEP}]
		dev-python/pysqlite3[${PYTHON_USEDEP}]
	')
"
BDEPEND=""

src_install() {
	dodoc CHANGELOG README.md INSTALL.md

	dosbin bin/btrfs.static
	python_doscript dduper
}
