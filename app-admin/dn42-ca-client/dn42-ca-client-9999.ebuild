# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="dn42 automatic CA client"
HOMEPAGE="https://wiki.dn42.us/services/Automatic-CA"
SRC_URI="https://ca.dn42/ca.dn42 -> ${PN}"

LICENSE="dn42-ca-client"
SLOT="0"
KEYWORDS=""

DEPEND="app-misc/ca-certificates-dn42"
RDEPEND="
	${DEPEND}
	app-crypt/gnupg
	dev-libs/openssl
	net-misc/curl
"
BDEPEND=""

S="${WORKDIR}"

src_unpack() {
	for a in "${A[@]}"; do
		cp "${DISTDIR}/${a}" "${S}" || die
	done
}

src_install() {
	exeinto /usr/bin
	doexe ${PN}
}

pkg_postinst() {
	ewarn "Please verify the installed script with app-crypt/gnupg!"
}
