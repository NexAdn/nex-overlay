# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

PYTHON_COMPAT=( python3_{8..10} )
inherit distutils-r1 git-r3

DESCRIPTION="Plugin for www-apps/netbox to support documentation of network tunnelling"
HOMEPAGE="https://github.com/jdrew82/netbox-tunnels-plugin"
SRC_URI=""
EGIT_REPO_URI="https://github.com/jdrew82/netbox-tunnels-plugin.git"

LICENSE="Apache-2.0"
SLOT="0"
KEYWORDS=""

DEPEND=""
RDEPEND="${DEPEND}"
BDEPEND=""

PATCHES=(
	"${FILESDIR}/${P}-not-install-tests.patch"
)
