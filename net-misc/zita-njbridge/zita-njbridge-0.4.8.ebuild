# Copyright 2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit toolchain-funcs

DESCRIPTION="Send Audio signals over IP networks"
HOMEPAGE="https://kokkinizita.linuxaudio.org/"
SRC_URI="https://kokkinizita.linuxaudio.org/linuxaudio/downloads/${P}.tar.bz2"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64"

DEPEND="
	media-libs/zita-resampler:=
"
RDEPEND="${DEPEND}"

src_compile() {
	emake -C source CXX="$(tc-getCXX)" PREFIX="${EPREFIX}/usr"
}

src_install() {
	local myemakeargs=(
		DESTDIR="${D}"
		PREFIX="${EPREFIX}/usr"
		LIBDIR="${EPREFIX}/usr/$(get_libdir)"
	)
	emake -C source "${myemakeargs[@]}" install

	einstalldocs
}
