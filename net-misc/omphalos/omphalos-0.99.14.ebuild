# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit cmake

DESCRIPTION="A tool for network enumeration and domination"
HOMEPAGE="https://nick-black.com/dankwiki/index.php/Omphalos https://github.com/dankamongmen/omphalos"
SRC_URI="https://github.com/dankamongmen/omphalos/archive/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="doc +tui +readline"

RDEPEND="
	dev-libs/libnl:3=
	net-libs/libpcap
	sys-libs/zlib:=
	net-wireless/wireless-tools
	sys-libs/libcap

	tui? (
		>=dev-cpp/notcurses-2.4.4
		<dev-cpp/notcurses-3
	)
	readline? (
		sys-libs/readline:=
	)
"
DEPEND="${RDEPEND}"
BDEPEND="
	doc? ( app-text/pandoc )
"

src_configure() {
	local mycmakeargs=(
		-DUSE_PANDOC=$(usex doc)
		-DUSE_NOTCURSES=$(usex tui)
		-DUSE_READLINE=$(usex readline)
		-DUSE_SUDO=false
	)
	cmake_src_configure
}

src_install() {
	default
	cmake_src_install

	insinto /usr/share/${PN}
	doins ieee-oui.txt
}
