# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="Multi-threaded BGP route collector written in C"
HOMEPAGE="https://github.com/spale75/piranha"
SRC_URI="https://github.com/spale75/piranha/archive/refs/tags/piranha-v${PV}.tar.gz"

LICENSE="Apache-2.0"
SLOT="0"
KEYWORDS="~amd64 ~x86"

DEPEND="
	acct-user/piranha
	acct-group/piranha
"
RDEPEND="${DEPEND}"
BDEPEND=""

PATCHES=(
	"${FILESDIR}/${PN}-configure-ignore-unknown-options.patch"
	"${FILESDIR}/${PN}-fix-makefile.patch"
	"${FILESDIR}/${P}-use-standard-paths.patch"
)

S="${WORKDIR}/${PN}-${PN}-v${PV}"

src_configure() {
	econf --verbose
}

src_install() {
	dobin bin/{piranha,piranhactl,ptoa}

	insinto /etc
	newins etc/piranha_sample.conf piranha.conf

	doman man/man1/{piranha,piranhactl,ptoa}.1
	doman man/man5/piranha.conf.5

	dodoc README.md

	keepdir /var/lib/piranha/dump
	fowners piranha:piranha /var/lib/piranha/dump

	keepdir /var/log/piranha
	fowners piranha:piranha /var/log/piranha
}
