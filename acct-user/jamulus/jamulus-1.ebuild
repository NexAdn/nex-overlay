# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit acct-user

DESCRIPTION="user for media-sound/jamulus"
ACCT_USER_ID=
ACCT_USER_GROUPS=( jamulus )

acct-user_add_deps
