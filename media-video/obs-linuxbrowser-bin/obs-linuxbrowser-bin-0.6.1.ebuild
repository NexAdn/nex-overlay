# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="Alternative browser source plugin for OBS Studio"
HOMEPAGE="https://github.com/bazukas/obs-linuxbrowser"
SRC_URI="https://github.com/bazukas/obs-linuxbrowser/releases/download/${PV}/linuxbrowser${PV}-obs23.0.2-64bit.tgz -> ${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="-* ~amd64"

DEPEND=""
RDEPEND=">=media-video/obs-studio-23.0.2"
BDEPEND=""

S="${WORKDIR}/obs-linuxbrowser"

src_install() {
	pushd bin/64bit
	insinto /usr/$(get_libdir)/obs-plugins/
	for f in *; do
		doins "$f"
	done
	popd

	pushd data
	insinto /usr/share/obs/obs-plugins/obs-linuxbrowser
	doins -r cef
	doins -r locale
}
