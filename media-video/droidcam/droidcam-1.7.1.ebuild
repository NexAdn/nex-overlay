# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit desktop

DESCRIPTION="Use your Android phone as a webcam"
HOMEPAGE="https://www.dev47apps.com/"
SRC_URI="https://github.com/dev47apps/${PN}/archive/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="gui"

DEPEND="
	app-pda/libusbmuxd:0/2.0-6
	media-libs/alsa-lib
	media-libs/libjpeg-turbo:=
	media-video/ffmpeg:=
	gui? ( x11-libs/gtk+:3 )
"
RDEPEND="
	${DEPEND}
	media-video/v4l2loopback
"
BDEPEND="
	virtual/pkgconfig
"

PATCHES=(
	"${FILESDIR}/fix-desktop.patch"
)

src_compile() {
	local make_target="droidcam-cli"
	if use gui; then
		make_target="all"
	fi

	emake \
		CC="${CXXFLAGS}" \
		JPEG_DIR="/usr" \
		USBMUXD="$(pkg-config --libs --cflags libusbmuxd-2.0)" \
		${make_target}
}

src_install() {
	if use gui; then
		dobin droidcam
	fi
	dobin droidcam-cli

	dodoc README.md

	if use gui; then
		mv icon2.png droidcam.png || die "Failed to rename icon file"
		doicon droidcam.png
		domenu droidcam.desktop
	fi

	insinto /usr/
}

pkg_postinst() {
	einfo "For Droidcam to work, the v4l2loopback kernel module needs"
	einfo "to be loaded."
}
