# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

# Building with ninja causes infinite CMake re-runs instead of building anything
CMAKE_MAKEFILE_GENERATOR="emake"
inherit cmake

DESCRIPTION="C++ library for working with MIME messages and email services"
HOMEPAGE="https://www.vmime.org/ https://github.com/kisli/vmime"
SRC_URI="https://github.com/kisli/vmime/archive/refs/tags/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="GPL-3" # TODO: OpenSSL exception
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="examples iconv +icu +sasl sendmail static-libs +ssl openssl +gnutls test"

REQUIRED_USE="ssl? ( ^^ ( openssl gnutls ) ) ^^ ( icu iconv )"

# TODO openssl/gnutls, icu/iconv
DEPEND="
	iconv? ( virtual/libiconv )
	icu? ( dev-libs/icu:= )

	sasl? ( virtual/gsasl )

	sendmail? ( virtual/mta )

	ssl? (
		gnutls? ( net-libs/gnutls:= )
		openssl? ( dev-libs/openssl:= )
	)
"
RDEPEND="${DEPEND}"
BDEPEND="
	test? ( dev-util/cppunit )
"

RESTRICT="!test? ( test )"

src_configure() {
	local mycmakeargs=(
		-DVMIME_BUILD_SHARED_LIBRARY=true
		-DVMIME_BUILD_STATIC_LIBRARY=$(usex static-libs true false)
		-DVMIME_BUILD_SAMPLES=$(usex examples true false)
		-DVMIME_HAVE_SASL_SUPPORT=$(usex sasl true false)
		-DVMIME_HAVE_MESSAGING_FEATURES=true
		-DVMIME_HAVE_MESSAGING_PROTO_SENDMAIL=$(usex sendmail true false)
		-DVMIME_HAVE_FILESYSTEM_FEATURES=true
		-DVMIME_HAVE_TLS_SUPPORT=$(usex ssl true false)
		-DVMIME_INSTALL_LIBDIR=/usr/$(get_libdir)
	)
	if use openssl; then
		mycmakeargs+=( -DVMIME_TLS_SUPPORT_LIB=openssl )
	fi
	if use gnutls; then
		mycmakeargs+=( -DVMIME_TLS_SUPPORT_LIB=gnutls )
	fi
	if use icu; then
		mycmakeargs+=( -DVMIME_CHARSETCONV_LIB=icu )
	fi
	if use iconv; then
		mycmakeargs+=( -DVMIME_CHARSETCONV_LIB=iconv )
	fi

	cmake_src_configure
}
