# Copyright 2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

# @ECLASS: non-build.eclass
# @MAINTAINER:
# Adrian Schollmeyer <nex+b-g-o@nexadn.de>
# @AUTHOR:
# Adrian Schollmeyer <nex+b-g-o@nexadn.de>
# @SUPPORTED_EAPIS: 6 7
# @BLURB: Eclass for building split packages from the Non suite
# @DESCRIPTION:
# This eclass provides common ebuild phase functions, SRC_URI and dependencies
# for building packages from the Non suite.

case ${EAPI} in
	6|7) ;;
	*) die "${ECLASS}: EAPI ${EAPI} unsupported."
esac

if [[ ! ${_NON_BUILD_ECLASS} ]]; then
_NON_BUILD_ECLASS=1

PYTHON_COMPAT=( python3_10 )
PYTHON_REQ_USE="threads(+)"
inherit python-any-r1 waf-utils

EXPORT_FUNCTIONS src_configure src_compile src_install pkg_postinst pkg_postrm

# @ECLASS_VARIABLE: NON_PROJECT
# @PRE_INHERIT
# @DESCRIPTION:
# Defines the name of the project to be built.
# May be set by an ebuild if the project name doesn't match the package name.
: ${NON_PROJECT:=${PN#non-}}

HOMEPAGE="https://non.tuxfamily.org/"
SRC_URI="https://github.com/linuxaudio/non/archive/refs/tags/non-daw-v${PV}.tar.gz"

LICENSE="GPL-2"
SLOT="0"

DEPEND="
	media-libs/liblo
	media-libs/ladspa-sdk
	media-libs/liblrdf
	virtual/jack
	x11-libs/ntk
	${PYTHON_DEPS}
"
RDEPEND="${DEPEND}"

S="${WORKDIR}/non-non-daw-v${PV}"

non-build_src_configure() {
	waf-utils_src_configure \
		--project="${NON_PROJECT}"
}

non-build_src_compile() {
	waf-utils_src_compile
}

non-build_src_install() {
	waf-utils_src_install

	# Non for some reason doesn't care what docdir is set
	mv -v \
		"${ED}"/usr/share/doc/non-${NON_PROJECT} \
		"${ED}"/usr/share/doc/${PF} \
		|| die
}

non-build_pkg_postinst() {
	xdg_icon_cache_update
}

non-build_pkg_postrm() {
	xdg_icon_cache_update
}

fi
