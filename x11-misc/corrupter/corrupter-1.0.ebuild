# Copyright 2020-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="Simple image glitcher suitable for producing nice looking lockscreens"
HOMEPAGE="https://github.com/r00tman/corrupter"
SRC_URI="https://github.com/r00tman/corrupter/archive/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="BSD"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

DEPEND="dev-lang/go"
RDEPEND="${DEPEND}"
BDEPEND=""

src_compile() {
	go build -o corrupter main.go || die
}

src_install() {
	default
	dobin corrupter
}
