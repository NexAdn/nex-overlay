# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="Script for fixing issues with suspend under elogind with nvidia-drivers"
HOMEPAGE="https://wiki.gentoo.org/wiki/No_homepage"
SRC_URI=""

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86"

DEPEND=""
RDEPEND="x11-drivers/nvidia-drivers"
BDEPEND=""

S="${WORKDIR}"

src_install() {
	exeinto /$(get_libdir)/elogind/system-sleep
	doexe "${FILESDIR}"/nvidia
}

pkg_postinst() {
	einfo "This fix only works with sys-auth/elogind installed"
}
