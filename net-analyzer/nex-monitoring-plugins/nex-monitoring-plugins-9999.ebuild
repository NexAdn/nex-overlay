# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit git-r3;

DESCRIPTION="Monitoring plugins by nex"
HOMEPAGE="https://gitlab.com/NexAdn/nex-monitoring-plugins"
SRC_URI=""
EGIT_REPO_URI="https://gitlab.com/NexAdn/nex-monitoring-plugins.git"

LICENSE="MIT"
SLOT="0"
KEYWORDS=""

DEPEND=""
RDEPEND="${DEPEND}"
BDEPEND=""

src_install() {
	exeinto /usr/$(get_libdir)/nagios/plugins
	for plugin in check_*; do
		doexe ${plugin}
	done
}
