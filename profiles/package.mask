# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

# Adrian Schollmeyer <nex@nexadn.de> (2021-08-05)
# net-misc/piranha seems to break some stuff upon startup, including sudo
net-misc/piranha
