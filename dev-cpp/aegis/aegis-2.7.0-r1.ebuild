# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit git-r3 cmake

DESCRIPTION="Discord library written in C++"
HOMEPAGE="https://github.com/zeroxs/aegis.cpp"
SRC_URI=""
EGIT_REPO_URI="https://github.com/zeroxs/aegis.cpp.git"

EGIT_SUBMODULES=()

EGIT_COMMIT="${PV}"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64"
IUSE="examples +static-libs"

DEPEND="
	sys-libs/zlib
	dev-cpp/aegis-asio
	dev-cpp/aegis-websocketpp
	>=dev-cpp/nlohmann_json-3.1.2
"
RDEPEND="${DEPEND}"
BDEPEND="
	>=dev-libs/openssl-1.0.1
	>=dev-libs/spdlog-1.4.0
"

PATCHES=(
	"${FILESDIR}/aegis-opt-paths.patch"
)

src_configure() {
	# Alternative install prefix to get rid of conflicts with the real websocketpp
	local mycmakeargs=(
		-DBUILD_EXAMPLES="$(usex examples)"
		-DBUILD_SHARED_LIBS="$(usex static-libs false true)"
		-DCMAKE_CXX_STANDARD=17
		-DCMAKE_INSTALL_PREFIX=/opt/aegis
	)
	cmake_src_configure
}
