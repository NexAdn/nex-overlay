# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit git-r3 cmake

DESCRIPTION="C++ websocket library (zeroxs fork)"
HOMEPAGE="https://github.com/zeroxs/websocketpp"
SRC_URI=""
EGIT_REPO_URI="https://github.com/zeroxs/websocketpp.git"

EGIT_COMMIT="ab7995c75f2b6af111f21028f333e81393bf28e7"

LICENSE="BSD"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND=""
RDEPEND="
	dev-libs/boost
"
BDEPEND=""

PATCHES=(
	"${FILESDIR}"/${P}-cmake-install.patch
)

src_configure() {
	local mycmakeargs=(
		-DENABLE_CPP11=ON
		-DBUILD_TESTS=OFF
		-DCMAKE_INSTALL_PREFIX=/opt/aegis
	)

	cmake_src_configure
}

src_install() {
	cmake_src_install
}
