# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit cmake;

DESCRIPTION="A type safe embedded domain specific language for SQL queries and results in C++"
HOMEPAGE="https://github.com/rbock/sqlpp11"
SRC_URI="https://github.com/rbock/${PN}/archive/${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="BSD-2"
SLOT="0"
KEYWORDS="~amd64"
IUSE="test"

DEPEND="
	>=dev-libs/date-3.0.0
"
RDEPEND="${DEPEND}"
BDEPEND=""

PATCHES="
	"${FILESDIR}/fix-cmake.patch"
"

src_configure() {
	local mycmakeargs=(
		-DBUILD_TESTING="$(usex test)"
	)
	cmake_src_configure
}
