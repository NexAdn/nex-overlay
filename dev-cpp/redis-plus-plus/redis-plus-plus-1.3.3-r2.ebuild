# Copyright 2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit cmake

DESCRIPTION="C++ hiredis wrapper library"
HOMEPAGE="https://github.com/sewenew/redis-plus-plus"
SRC_URI="https://github.com/sewenew/redis-plus-plus/archive/refs/tags/${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="Apache-2.0"
SLOT="0/$(ver_cut 1)"
KEYWORDS="~amd64"
IUSE="+ssl static-libs"

DEPEND="
	dev-libs/hiredis:=

	ssl? ( dev-libs/hiredis:=[ssl] )
"
RDEPEND="${DEPEND}"
BDEPEND=""

PATCHES=(
	"${FILESDIR}/cmake-use-gnuinstalldirs.patch"
	"${FILESDIR}/cmake-set-module-path.cmake"
)

src_configure() {
	local mycmakeargs=(
		-DREDIS_PLUS_PLUS_USE_TLS=$(usex ssl)
		-DREDIS_PLUS_PLUS_BUILD_STATIC=$(usex static-libs)
		-DREDIS_PLUS_PLUS_BUILD_SHARED=true
		-DREDIS_PLUS_PLUS_BUILD_TEST=false
	)

	cmake_src_configure
}

src_install() {
	cmake_src_install

	insinto /usr/$(get_libdir)/cmake/redis++
	doins "${FILESDIR}"/Findhiredis{,_ssl}.cmake
}
