include(FindPackageHandleStandardArgs)

find_package(PkgConfig REQUIRED)
pkg_check_modules(hiredis_ssl REQUIRED hiredis_ssl)

find_package_handle_standard_args(hiredis_ssl
	REQUIRED_VARS
		hiredis_ssl_LIBRARIES
		hiredis_ssl_INCLUDE_DIRS
)
