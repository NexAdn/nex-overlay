diff --git a/cmake/redis++-config.cmake.in b/cmake/redis++-config.cmake.in
index 242f473..326c025 100644
--- a/cmake/redis++-config.cmake.in
+++ b/cmake/redis++-config.cmake.in
@@ -1,5 +1,7 @@
 @PACKAGE_INIT@
 
+list(APPEND CMAKE_MODULE_PATH ${CMAKE_CURRENT_LIST_DIR})
+
 include(CMakeFindDependencyMacro)
 
 string(REPLACE "," ";" REDIS_PLUS_PLUS_DEPENDS_LIST @REDIS_PLUS_PLUS_DEPENDS@)
@@ -10,3 +12,5 @@ endforeach()
 include("${CMAKE_CURRENT_LIST_DIR}/redis++-targets.cmake")
 
 check_required_components(redis++)
+
+list(REMOVE_AT CMAKE_MODULE_PATH -1)
