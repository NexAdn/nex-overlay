include(FindPackageHandleStandardArgs)

find_package(PkgConfig REQUIRED)
pkg_check_modules(hiredis REQUIRED hiredis)

find_package_handle_standard_args(hiredis
	REQUIRED_VARS
		hiredis_LIBRARIES
		hiredis_INCLUDE_DIRS
)
