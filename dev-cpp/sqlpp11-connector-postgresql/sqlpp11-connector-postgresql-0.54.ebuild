# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit cmake;

DESCRIPTION="PostgreSQL connector for dev-cpp/sqlpp11"
HOMEPAGE="https://github.com/matthijs/sqlpp11-connector-postgresql"
SRC_URI="https://github.com/matthijs/sqlpp11-connector-postgresql/archive/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="BSD-2"
SLOT="0"
KEYWORDS="~amd64"

DEPEND="
	dev-cpp/sqlpp11:=
	dev-db/postgresql:=
"
RDEPEND="${DEPEND}"
BDEPEND=""

PATCHES=(
	"${FILESDIR}/fix-build.patch"
)

src_configure() {
	local mycmakeargs=(
		-DENABLE_TESTS="false"
	)
	cmake_src_configure
}
