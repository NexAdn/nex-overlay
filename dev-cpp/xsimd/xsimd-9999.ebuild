# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit cmake git-r3

DESCRIPTION="C++ wrappers for SIMD intrinsics and optimized mathematical functions"
HOMEPAGE="https://github.com/xtensor-stack/xsimd https://xsimd.readthedocs.io/en/latest/"
SRC_URI=""
EGIT_REPO_URI="https://github.com/xtensor-stack/xsimd.git"

LICENSE="BSD"
SLOT="0"
KEYWORDS=""
IUSE="xtl-complex"

DEPEND=""
RDEPEND="${DEPEND}"
BDEPEND=""

RESTRICT="test"

src_configure() {
	local mycmakeargs=(
		-DENABLE_XTL_COMPLEX="$(usex xtl-complex)"
	)
	cmake_src_configure
}
